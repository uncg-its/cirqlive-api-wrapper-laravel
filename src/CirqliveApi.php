<?php

namespace Uncgits\CirqliveApiLaravel;

use Illuminate\Support\Facades\Cache;
use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Support\Facades\Log;

class CirqliveApi extends \Uncgits\CirqliveApi\CirqliveApi
{
    protected $historyMonths;

    protected $cacheActive;
    protected $endpointsToCache;
    protected $cacheMinutes;

    protected $debugMode;

    public function __construct()
    {

        // set vars specific to this instance of Laravel
        $this->setApiHost(config('cirqlive-api.api_host'));
        $this->setUsername(config('cirqlive-api.username'));
        $this->setPassword(config('cirqlive-api.password'));
        $this->setAuthMethod(config('cirqlive-api.auth_method'));

        if (config('cirqlive-api.http_proxy.enabled') == 'true') {
            $this->setUseProxy(true);
            $this->setProxyHost(config('cirqlive-api.http_proxy.host'));
            $this->setProxyPort(config('cirqlive-api.http_proxy.port'));
        }

        // cache configuration
        $this->cacheActive = config('cirqlive-api.cache_active');
        $this->endpointsToCache = config('cirqlive-api.cache_endpoints');
        $this->cacheMinutes = config('cirqlive-api.cache_minutes');

        // debug mode (not enabled in production)
        $this->debugMode = (config('app.env') == 'production') ? false : config('cirqlive-api.debug_mode');
    }

    /**
     * apiCall() - performs a call to the WebEx XML API
     *
     * @param string $function - function / endpoint (per CirQlive API docs)
     * @param string $method - HTTP request method (e.g. GET, POST, etc.)
     * @param array $params (optional) - Parameters (for GET), or body content (for POST et. al.)
     * @param boolean $download (optional) - whether the call should expect to get a file download. If false, the call expects JSON.
     *
     * @return array
     */

    protected function apiCall($function, $method, $params = [], $download = false)
    {
        $isDownload = $download ? "download" : "no-download";
        $hashedCacheKey = hash('sha256', $function . $method . json_encode($params) . $isDownload);
        $this->debugMessage('Hashed cache key: ' . $hashedCacheKey);

        if ($this->cacheActive && Cache::has($hashedCacheKey)) {
            // in cache
            $resultArray = Cache::get($hashedCacheKey);
            $returnArray = [
                'source'  => 'cache',
                'created' => Cache::get($hashedCacheKey . '-created', 'unknown')
            ];
            $message = 'API data for ' . $function . ' retrieved from cache (created: ' . $returnArray['created'] . ')';
            $messageLevel = 'success';
        } else {
            // not in cache or cache inactive
            $this->debugStartMeasure('apiCall', 'Making API call to CirQlive');
            $resultArray = parent::apiCall($function, $method, $params, $download);
            $this->debugStopMeasure('apiCall');

            $created = time();

            // cache if the call was successful, and we are supposed to cache it...
            if ($resultArray['response']['httpCode'] == 200) {
                $messageLevel = 'success';
                $message = 'API call to ' . $function . ' successful.';

                if ($this->cacheActive) {
                    if (in_array($function, $this->endpointsToCache)) {
                        Cache::put($hashedCacheKey, $resultArray, $this->cacheMinutes);
                        Cache::put($hashedCacheKey . '-created', $created, $this->cacheMinutes);
                        $message .= ' Result cached in API cache.';
                    } else {
                        $message .= ' Not a cacheable API call.';
                    }
                }
            } else {
                $message = 'API error calling ' . $function . ': ' . $resultArray['response']['httpCode'] . ' - ' . $resultArray['response']['httpReason'] . ' (' . $resultArray['response']['message'] . ')';
                $messageLevel = 'danger';
            }
            $returnArray = [
                'source'  => 'api',
                'created' => $created
            ];
        }

        // handle notifications / logging / etc.
        $notificationMode = config('cirqlive-api.notification_mode');

        if (in_array('flash', $notificationMode)) {
            flash($message, $messageLevel);
        }

        if (in_array('log', $notificationMode)) {
            if ($messageLevel == 'danger') {
                Log::error($message);
            } else {
                Log::info($message);
            }
        }

        $finalArray = array_merge($returnArray, $resultArray);

        $this->debugInfo($finalArray);

        return array_merge($finalArray);
    }

    // debug helpers

    protected function debugMessage($message, $label = null)
    {
        if ($this->debugMode) {
            if (!is_null($label)) {
                Debugbar::addMessage($message, $label);
            } else {
                Debugbar::addMessage($message);
            }
        }
    }

    protected function debugInfo($object)
    {
        if ($this->debugMode) {
            Debugbar::info($object);
        }
    }

    protected function debugStartMeasure($key, $label = '')
    {
        if ($this->debugMode) {
            Debugbar::startMeasure($key, $label);
        }
    }

    protected function debugStopMeasure($key)
    {
        if ($this->debugMode) {
            Debugbar::stopMeasure($key);
        }
    }


//    public function getRecordings($options = []) {
//        if (!isset($options['historyMonths'])) {
//            $options['historyMonths'] = $this->historyMonths;
//        }
//
//        return parent::getRecordings($options);
//    }
//
//    public function getPastSessions($options = []) {
//        if (!isset($options['historyMonths'])) {
//            $options['historyMonths'] = $this->historyMonths;
//        }
//
//        return parent::getPastSessions($options);
//    }
}
