# CirQlive API Library - Laravel Wrapper

Contact: matt.libera@uncg.edu

# Introduction

This package is a Laravel wrapper for the UNCG CirQlive API PHP Library package, so that the CirQlive API PHP Library can be used in Laravel apps.

> This is a **work in progress**. Not recommend for production apps just yet.

---

# Installation

1. `composer require 'uncgits/cirqlive-api-wrapper-laravel'`
2. Add `Uncgits\CirqliveApiLaravel\ServiceProvider::class,` to your `config/app.php` file
3. Run `php artisan vendor:publish --provider='Uncgits\CirqliveApiLaravel\ServiceProvider` - to publish the `cirqlive-api.php` config file
4. Set your environment credentials in your `.env` file, and set your configuration options in `config/cirqlive-api.php`

> Note: do not use protocols (http:// or https://) for `CIRQLIVE_API_HOST`

```
CIRQLIVE_API_HOST=

CIRQLIVE_API_USERNAME=
CIRQLIVE_API_PASSWORD=
CIRQLIVE_API_AUTH_METHOD=
```

## Dependencies

This package has dependencies on `uncgits/cirqlive-api-php-library` and `barryvdh/laravel-debugbar` (dev)

---

# Usage

## Basic Usage / Getting Started

In your code, assuming you've set your information/credentials properly in your `.env` file, you should be able to instantiate the `Uncgits\CirqliveApiLaravel\CirqliveApi` class, and then use any of its available methods (inherited from `Uncgits\CirqliveApi\CirqliveApi`) to make an API call.

## Configuration options

### Notification Mode

There are two notification options built into this library. You may flash the information on the screen as a flash message, or log it in the Laravel logs (whatever you've got set).

In the `config/cirqlive-api.php` file, `notification_mode` is an array. Inside, add one or both of `flash` and `log`.

If you choose to roll this into a higher-level custom notification / logging system for your app, you can leave this array empty to disable both of these methods, and then handle your own logging based on the return array from the API call (the API class returns the entire response, including response code, from every call).

> As of version 0.7, no flash messaging package is included. You are free to install your own - recommend `laracasts/flash` or something that implements a similar API.

### Debug Mode

_Requires DebugBar by Barryvdh (`barryvdh\laravel-debugbar`)_

If you turn on Debug Mode in the `config/cirqlive-api.php` file, extra information about the call will be written to the Debugbar. This includes timing, hashed cache key, and full results from each call.

 > Note: in accordance with best practices for this package, this will only work if the app environment is not set to `production`

## Caching

For speed, adherence to rate limits, and overall performance, caching is built into this library. This utilizes the Laravel Cache mechanism (which, like Logs, can be set however you want in the Laravel config). You can optionally disable this by adding `CIRQLIVE_API_CACHING=off` in your `.env` file.

### Cache TTL

The TTL of the cache is set to 10 minutes by default, but by adding `CIRQLIVE_API_CACHE_MINUTES=x` to your `.env` file, you can set the cache to expire after x minutes.

### Cached endpoints

Caching is performed only for specific endpoints (e.g. requests that would equate to typical `GET` requests). Obviously, you would not want to cache requests that are designed to actually modify information in CirQlive (equivalent to `POST` or `PATCH` or `DELETE` requests).

By default, there is a basic set of endpoints set up in `config/cirqlive-api.php`. If you would like change which endpoints are / are not cached (or add your own), simply modify the `cached_endpoints` array in the config file.

---

# Version History

## 0.3.1

- change to PSR-4 declaration since we were already following it

## 0.3

- Fixes config keys for HTTP proxy

> NOTE: if upgrading, make sure you add the new config keys into your `cirqlive-api.php` config file, and set them in your `.env` file if using a proxy.

## 0.2.1

- Change `env()` calls to `config()` calls so that config caching can be used.

## 0.2

- Removes `standaniels/flash` as a dependency. Optional to install a flash package.

## 0.1.1

- Corrected `composer.json` to require the CirQlive API library.

## 0.1

- First real release.
- `.env` integration for CirQlive credentials
- Debug mode
- Caching
